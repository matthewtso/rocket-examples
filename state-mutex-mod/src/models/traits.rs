use super::DbConn;
use rusqlite::Error;

pub trait Model {
    fn new(DbConn) -> Self where Self: Sized;
    fn get_name(&self, i64) -> Result<String, Error>;
}

pub trait Modelable {
    fn get_table_name<'a>() -> &'a str;
//    fn get_rows() -> RowInfo;
}

/// Gettable enforces the ability to get an item.
pub trait Gettable<T> {
//where T: Modelable {
    fn get(&self, &T) -> Result<T, Error>;
}

pub trait Creatable<T> {
    fn create(&self, &T) -> Result<T, Error>;
}

pub trait Updatable<T> {
    fn update(&self, &T) -> Result<T, Error>;
}

pub trait Removable<T> {
    fn remove(&self, &T) -> Result<T, Error>;
}
