use rusqlite::Connection;
use std::sync::Mutex;
use std::sync::Arc;
use rocket::Rocket;

mod traits;

// Declare models.
mod user;
mod toy;

pub use self::traits::Model;

pub use self::user::UserModel;
pub use self::toy::ToyModel;

// Alias protected connection type.
type DbConn = Arc<Mutex<Connection>>;

pub fn add_to(app: Rocket) -> Rocket {
    let connection = Connection::open_in_memory().unwrap();

    // Initialize database connection.
    connection.execute(r#"CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL )
    "#, &[]).unwrap();

    connection.execute(r#"CREATE TABLE toys (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL )
    "#, &[]).unwrap();

    connection.execute(r#";
    INSERT INTO users (name)
              SELECT 'Matthew'
    UNION ALL SELECT 'Elias'
    UNION ALL SELECT 'Dan'
    UNION ALL SELECT 'Chris'
    UNION ALL SELECT 'Alex'
    UNION ALL SELECT 'Toni'
    UNION ALL SELECT 'John'"#, &[]).unwrap();

    connection.execute(r#";
    INSERT INTO toys (name)
              SELECT 'gameboy'
    UNION ALL SELECT 'wii'
    UNION ALL SELECT 'top'
    UNION ALL SELECT 'fidget spinner'
    UNION ALL SELECT 'dice'
    UNION ALL SELECT 'playing cards'
    UNION ALL SELECT 'rubber ducky'"#, &[]).unwrap();

    // Protect with a mutex.
    let connection =  Arc::new(Mutex::new(connection));

    let users = UserModel::new(connection.clone());
    let toys = ToyModel::new(connection.clone());

    let app = app.manage(users);
    let app = app.manage(toys);

    app
}
