use super::DbConn;
use super::Model;
use rusqlite::Error;

pub struct ToyModel {
    conn: DbConn,
}

impl Model for ToyModel {
    fn new(conn: DbConn) -> ToyModel {
        let conn = conn.clone();

        ToyModel {
            conn,
        }
    }

    fn get_name(&self, id: i64) -> Result<String, Error> {
        self.conn.lock()
            .expect("db lock")
            .query_row(
                "SELECT id, name FROM toys WHERE id = ?1",
                &[ &id ],
                |row| {
                    let name: String = row.get(1);

                    name
                })
    }
}