use super::DbConn;
use super::Model;
use rusqlite::Error;

pub struct UserModel {
    conn: DbConn,
}

impl Model for UserModel {
    fn new(conn: DbConn) -> UserModel {
        let conn = conn.clone();

        UserModel {
            conn,
        }
    }

    fn get_name(&self, id: i64) -> Result<String, Error> {
        self.conn.lock()
            .expect("db lock")
            .query_row(
                "SELECT id, name FROM users WHERE id = ?1",
                &[ &id ],
                |row| {
                    let name: String = row.get(1);

                    name
                })
    }
}
