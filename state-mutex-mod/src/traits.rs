use rocket::error::LaunchError;

// Declare server interface.
pub type StartError = LaunchError;

pub trait Runnable {
    fn new() -> Self;
    fn start(self) -> StartError;
}
