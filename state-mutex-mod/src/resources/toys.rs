use rocket::State;
use models::Model;
use models::ToyModel;

#[get("/toy/<id>")]
pub fn toy_name(model: State<ToyModel>, id: i64) -> Option<String> {
    match model.get_name(id) {
        Ok(name) => Some(format!("{}\n", name)),
        Err(_) => None,
    }
}
