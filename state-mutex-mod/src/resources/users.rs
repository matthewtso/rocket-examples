use rocket::State;
use models::Model;
use models::UserModel;

#[get("/user/<id>")]
pub fn user_name(model: State<UserModel>, id: i64) -> Option<String> {
    match model.get_name(id) {
        Ok(name) => Some(format!("{}\n", name)),
        Err(_) => None,
    }
}

//!    With a processor layer, we can do the following:
//!    match processors::user::validate_new(&user) {
//!        Ok(user) => match model.insert(&user) => {
//!            Ok(user) => Ok(Json(json!(user))),
//!            Err(e) => Failure(Status::BadRequest),
//!        },
//!        Err(e) => Failure(e.status, e.message),
//!    }
//!    AND/OR
//!    A custom route guard layer to act as validation layer first
//!    #[post("/user", data = "<new_user>")]
//!    pub fn create(model: State<UserModel>, new_user: NewUserRequest) -> Result<Json<Value>, rocket::Outcome> { }