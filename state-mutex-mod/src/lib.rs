#![feature(plugin, decl_macro)]
#![plugin(rocket_codegen)]

// Import libraries.
extern crate rocket;
extern crate rusqlite;

// Expose Runnable implementation.
pub use rocket::Rocket;

mod models;
mod resources;
mod traits;

// Export server interface.
pub use traits::StartError;
pub use traits::Runnable;

/// Route info is under the following names after hit with codegen.
///     resource::static_rocket_route_info_for_toy_name
///     resources::static_rocket_route_info_for_user_name
/// Thus, we import everything so that we don't explicitly specify these generated
/// routes and cause pain in the future when updating resource names.
use resources::*;

impl Runnable for rocket::Rocket {
    fn new() -> rocket::Rocket {
        let app = rocket::ignite()
            .mount("/", routes![ user_name, toy_name ]);

//        let app = routes::mount(app);

        let app = models::add_to(app);

//        for model in models::get() {
//            app = app.manage(model);
//        }

        app
    }

    fn start(self) -> StartError {
        self.launch()
    }
}
