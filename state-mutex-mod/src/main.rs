extern crate rocket_state_mod;

use rocket_state_mod::Runnable;
use rocket_state_mod::Rocket;

fn main() {
    Rocket::new().start();
}