# rocket-state

Demo a shared in-memory database connection between multiple Rocket-managed state structs.

## Notes

- `Arc` is used to give (static? must be for `rocket::State`) pointers to the Model structs.
- `Mutex<Connection>` is shared among the models (I think).
- What will it take to improve db cell reads by managing the db connection with `RwLock`?
    - Why this when using `RwLock`:
      ```
      `std::cell::RefCell<lru_cache::LruCache<std::string::String, rusqlite::raw_statement::RawStatement>>` cannot be shared between threads safely
      ```
- It may make more sense to put each module's traits in the module file (`lib.rs`
  for the top level and `mod.rs` for submodules) in order to keep the traits
  (and enums, typealiases, etc) in a visible location. While it may produce cleaner
  files by placing each group of these in a grouped file (like `traits.rs` for traits),
  some IDE's do not support pinning these files to the top of the file list,
  instead interspersing them in between the rest of the module's logic files.
  However, there is a case to be made that traits, enums, structs, etc. should
  not be grouped in a module, but as constructs that should instead organically
  support the structure of the program.

## Test Endpoints

```bash
curl localhost:2077/user/1 & \
curl localhost:2077/user/2 & \
curl localhost:2077/user/3 & \
curl localhost:2077/user/4 & \
curl localhost:2077/toy/1 & \
curl localhost:2077/toy/2 & \
curl localhost:2077/toy/3 & \
curl localhost:2077/toy/4 & \
wait
```
