# rocket-state

Demo a shared in-memory database connection between multiple Rocket-managed state structs.

## Notes

- `Arc` is used to give (static? must be for `rocket::State`) pointers to the Model structs.
- `Mutex<Connection>` is shared among the models (I think).
- What will it take to improve db cell reads by managing the db connection with `RwLock`?
    - Why this when using `RwLock`:
      ```
      `std::cell::RefCell<lru_cache::LruCache<std::string::String, rusqlite::raw_statement::RawStatement>>` cannot be shared between threads safely
      ```

## Test Endpoints

```bash
curl localhost:2077/user/1 & \
curl localhost:2077/user/2 & \
curl localhost:2077/user/3 & \
curl localhost:2077/user/4 & \
curl localhost:2077/toy/1 & \
curl localhost:2077/toy/2 & \
curl localhost:2077/toy/3 & \
curl localhost:2077/toy/4 & \
wait
```