#![feature(plugin, decl_macro)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rusqlite;

use rocket::State;
use std::sync::Mutex;
use rusqlite::Connection;
use rusqlite::Error;
use std::sync::Arc;

struct UserModel {
    conn: Arc<Mutex<Connection>>,
}

struct ToyModel {
    conn: Arc<Mutex<Connection>>,
}

impl UserModel {
    fn new(conn: &Arc<Mutex<Connection>>) -> UserModel {
        let conn = conn.clone();

        UserModel {
            conn,
        }
    }

    fn get_name(&self, id: i64) -> Result<String, Error> {
        self.conn.lock()
            .expect("db lock")
            .query_row(
                "SELECT id, name FROM users WHERE id = ?1",
                &[ &id ],
                |row| {
                    let name: String = row.get(1);

                    name
                })
    }
}

impl ToyModel {
    fn new(conn: &Arc<Mutex<Connection>>) -> ToyModel {
        let conn = conn.clone();

        ToyModel {
            conn,
        }
    }

    fn get_name(&self, id: i64) -> Result<String, Error> {
        self.conn.lock()
            .expect("db lock")
            .query_row(
                "SELECT id, name FROM toys WHERE id = ?1",
                &[ &id ],
                |row| {
                    let name: String = row.get(1);

                    name
                })
    }
}

#[get("/user/<id>")]
fn user_name(model: State<UserModel>, id: i64) -> Option<String> {
    match model.get_name(id) {
        Ok(name) => Some(format!("{}\n", name)),
        Err(_) => None,
    }
}

#[get("/toy/<id>")]
fn toy_name(model: State<ToyModel>, id: i64) -> Option<String> {
    match model.get_name(id) {
        Ok(name) => Some(format!("{}\n", name)),
        Err(_) => None,
    }
}

fn main() {
    let connection = Connection::open_in_memory().unwrap();

    connection.execute(r#"CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL )
    "#, &[]).unwrap();

    connection.execute(r#"CREATE TABLE toys (
    id INTEGER PRIMARY KEY,
    name VARCHAR NOT NULL )
    "#, &[]).unwrap();

    connection.execute(r#";
    INSERT INTO users (name)
              SELECT 'Matthew'
    UNION ALL SELECT 'Elias'
    UNION ALL SELECT 'Dan'
    UNION ALL SELECT 'Chris'
    UNION ALL SELECT 'Alex'
    UNION ALL SELECT 'Toni'
    UNION ALL SELECT 'John'"#, &[]).unwrap();

    connection.execute(r#";
    INSERT INTO toys (name)
              SELECT 'gameboy'
    UNION ALL SELECT 'wii'
    UNION ALL SELECT 'top'
    UNION ALL SELECT 'fidget spinner'
    UNION ALL SELECT 'dice'
    UNION ALL SELECT 'playing cards'
    UNION ALL SELECT 'rubber ducky'"#, &[]).unwrap();

    let conn =  Arc::new(Mutex::new(connection));

    rocket::ignite()
        .manage(UserModel::new(&conn))
        .manage(ToyModel::new(&conn))
        .mount("/", routes![ user_name, toy_name ])
        .launch();
}
