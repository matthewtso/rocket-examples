# ignite-mut

A rocket project to demonstrate how to re-use the identifier that captures
the `rocket::Rocket` instance returned from `rocket::ignite()`.

1. Multiple `let` bindings.
2. Mutable identifier bindings if it needs to be used within another scope (like a for loop).
