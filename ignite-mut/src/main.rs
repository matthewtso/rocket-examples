#![feature(decl_macro, plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

use rocket::Rocket;

fn make_rocket() -> Rocket {
    rocket::ignite()
}

#[get("/")]
fn root() -> String {
    "hello\n".to_string()
}

#[get("/test")]
fn test() -> String {
    "test\n".to_string()
}

///! Declare app identifier as mutable in order to assign to it inside the for loop.
fn add_routes(mut app: Rocket) -> Rocket {
    let routes = vec![ routes![ root ], routes![ test ] ];

    for route in routes.iter() {
        app = app.mount("/", route.to_vec());
    }

    app
}

fn main() {
    //! Because rocket::Rocket's methods consume app,
    //! use let binding to re-use the name and capture
    //! the returned value.
    let app = make_rocket();

    let app = add_routes(app);

    app.launch();
}
